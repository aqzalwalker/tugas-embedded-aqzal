return rfsm.state {

   tutup = rfsm.state{ entry=function() print("Tutup gerbang") end },
   buka = rfsm.state{ entry=function() print("Buka gerbang") end },

   rfsm.transition { src='initial', tgt='tutup' },
   rfsm.transition { src='tutup', tgt='buka', events={ 'e_done' } },
   rfsm.transition { src='buka', tgt='tutup', events={ 'e_done' } },
}

